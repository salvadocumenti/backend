defmodule Backend.Repo.Migrations.CreateDocuments do
  use Ecto.Migration

  def change do
    create table(:documents) do
      add :title, :string
      add :description, :text
      add :amount, :integer
      add :date, :date
      add :category_id, references(:categories, on_delete: :delete_all), null: false
      timestamps()
    end

  end
end
