# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Backend.Repo.insert!(%Backend.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
c1 = Backend.Repo.insert!(%Backend.Category{title: "Scontrini medici"})
c2 = Backend.Repo.insert!(%Backend.Category{title: "Garanzia acquisti"})

Backend.Repo.insert!(%Backend.Document{
    title: "Televisore",
    description: "Nuovo TV super-mega-HD",
    date: ~D[2017-08-21],
    amount: Money.new(50000, :EUR),
    category: c2
})

Backend.Repo.insert!(%Backend.Document{
    title: "Maalox",
    description: "Contro la vomitella",
    date: ~D[2017-08-24],
    amount: Money.new(270, :EUR),
    category: c1
})
