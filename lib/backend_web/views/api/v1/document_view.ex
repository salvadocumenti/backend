defmodule BackendWeb.Api.V1.DocumentView do
    use BackendWeb, :view
    alias Backend.Attachment

    def render("index.json", %{documents: documents}) do
        %{data: render_many(documents, __MODULE__, "document.json")}
    end

    def render("show.json", %{document: document}) do
        %{data: render_one(document, __MODULE__, "document.json")}
    end

    def render("document.json", %{document: document}) do
        %{
            id: document.id,
            title: document.title,
            description: document.description,
            amount: document.amount,
            date: document.date,
            category_id: document.category.id,
            category: render_one(document.category, BackendWeb.Api.V1.CategoryView, "category.json"),
            attachment: Attachment.get_path(document),
            attachment_thumb: Attachment.get_thumb_path(document)
        }
    end
end
