defmodule BackendWeb.Api.V1.DocumentController do
  use BackendWeb, :controller
  alias Backend.Repo
  alias Backend.Document
  alias BackendWeb.Api.V1.ChangesetView
  alias Backend.Attachment

  def index(conn, _params) do
      documents = Repo.all(Document) |> Repo.preload(:category)
      render conn, "index.json", documents: documents
  end

  def show(conn, %{"id" => id}) do
      document = Repo.get!(Document, id) |> Repo.preload(:category)
      render conn, "show.json", document: document
  end

  def create(conn, document_params = %{"category_id" => category_id}) do
    changeset = Document.changeset(%Document{}, document_params)

    case Repo.insert(changeset) do
      {:ok, document} ->
        document = Repo.preload(document, :category)
        Attachment.save_file!(document.id, document_params)
        conn
        |> put_status(:created)
        |> render "show.json", document: document
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(ChangesetView, "error.json", changeset: changeset)
    end
  end

  def update(conn, %{"id" => id, "document" => document_params}) do
    document = Repo.get!(Document, id) |> Repo.preload(:category)
    changeset = Document.changeset(document, document_params)

    case Repo.update(changeset) do
      {:ok, document} ->
        Attachment.save_file!(document.id, document_params)
        conn
        |> put_status(:ok)
        |> render "show.json", document: document
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(ChangesetView, "error.json", changeset: changeset)
    end
  end
end
