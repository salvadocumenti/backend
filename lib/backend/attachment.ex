import Mogrify

defmodule Backend.Attachment do
  def get_path(document) do
    if File.exists?("media/#{document.id}/attachment.jpg") do
      "/uploads/#{document.id}/attachment.jpg"
    else
      nil
    end
  end

  def get_thumb_path(document) do
    if File.exists?("media/#{document.id}/thumb.jpg") do
      "/uploads/#{document.id}/thumb.jpg"
    else
      nil
    end
  end

  def save_file!(document_id, %{"attachment" => attachment}) do
    if attachment do
      File.mkdir_p!("media/#{document_id}")
      # Image
      image = open(attachment.path) |> format("jpg") |> save
      File.cp!(image.path, "media/#{document_id}/attachment.jpg")
      # Thumb
      thumb = open(attachment.path) |> format("jpg") |> resize("100x100") |> save
      File.cp!(thumb.path, "media/#{document_id}/thumb.jpg")
    end
  end
end
