defmodule Backend.Category do
  use Ecto.Schema
  import Ecto.Changeset
  alias Backend.{Category, Document}


  schema "categories" do
    field :title, :string
    has_many :documents, Document
    timestamps()
  end

  @doc false
  def changeset(%Category{} = category, attrs) do
    category
    |> cast(attrs, [:title])
    |> unique_constraint(:title)
    |> validate_required([:title])
  end
end
