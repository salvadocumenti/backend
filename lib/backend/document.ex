defmodule Backend.Document do
  use Ecto.Schema
  import Ecto.Changeset
  alias Backend.{Category, Document}

  schema "documents" do
    field :amount, Money.Ecto.Type
    field :date, :date
    field :description, :string
    field :title, :string
    belongs_to :category, Category
    timestamps()
  end

  @doc false
  def changeset(%Document{} = document, attrs) do
    document
    |> cast(attrs, [:title, :description, :amount, :date, :category_id])
    |> validate_required([:title, :description, :amount, :date, :category_id])
  end
end
