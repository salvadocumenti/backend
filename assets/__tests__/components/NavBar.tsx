import { shallow } from "enzyme";
import * as React from "react";

import NavBar from "../../js/app/components/NavBar";

describe("NavBar", () => {
    test("it has 2 entries", () => {
        const component = shallow((
            <NavBar />
        ));

        expect(component.find("ul.left li")).toHaveLength(2);
    });
});
