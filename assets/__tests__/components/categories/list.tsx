import { shallow } from "enzyme";
import * as React from "react";

import CategoriesList from "../../../js/app/components/categories/list";
import { ICategory } from "../../../js/app/components/categories/interfaces";
import { categories } from "../../fixtures/category";

const updateCategory = (category: ICategory, onSuccess?: () => void) => jest.fn();
const deleteCategory = (id: number) => jest.fn();

describe("NavBar", () => {
    test("it has 2 entries", () => {
        const component = shallow((
            <CategoriesList
                categories={categories}
                updateCategory={updateCategory}
                deleteCategory={deleteCategory}
            />
        ));

        expect(component.find("Category")).toHaveLength(categories.length);
    });
});
