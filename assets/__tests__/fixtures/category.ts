import { ICategory } from "../../js/app/components/categories/interfaces";

export const category1: ICategory = {
    id: 1,
    title: "Test Category 1",
};

export const category2: ICategory = {
    id: 2,
    title: "Test Category 2",
};

export const category3: ICategory = {
    id: 3,
    title: "Test Category 3",
};

export const categories: ICategory[] = [category1, category2, category3];
