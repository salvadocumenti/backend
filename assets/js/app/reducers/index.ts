import { combineReducers } from "redux";

import categories from "./categories";
import documents from "./documents";
import status from "./status";

export default combineReducers({
  categories, // same as categories: categories(state.categories, action)
  documents,
  status,
});
