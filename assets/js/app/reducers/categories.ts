import { CATEGORIES_LOADED } from "../actions/categories";

export default (state = [], action) => {
  switch (action.type) {
    case CATEGORIES_LOADED:
      return action.categories;
    default:
      return state;
  }
};
