import { CATEGORIES_LOADED  } from "../actions/categories";
import { DOCUMENTS_LOADED  } from "../actions/documents";
import { STATUS_LOADING, STATUS_NOT_LOADING } from "../actions/status";

export default (state = {}, action) => {
  switch (action.type) {
    case STATUS_LOADING:
      return { loading: true };
    case STATUS_NOT_LOADING:
    case CATEGORIES_LOADED:
    case DOCUMENTS_LOADED:
      return { loading: false };
    default:
      return state;
  }
};
