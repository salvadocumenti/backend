import { DOCUMENTS_LOADED } from "../actions/documents";

export default (state = [], action) => {
  switch (action.type) {
    case DOCUMENTS_LOADED:
      return action.documents;
    default:
      return state;
  }
};
