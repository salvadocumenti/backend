import * as React from "react";
import * as ReactDom from "react-dom";
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";

import Main from "./components/Main";
import App from "./reducers";

const initialState = {
  categories: [],
  status: {
    loading: false,
  },
};

const store = createStore(App, initialState, applyMiddleware(thunk));

const initApp = () => {
  ReactDom.render((
    <Provider store={store}>
      <Main/>
    </Provider>),
    document.getElementById("app"),
  );
};

export default initApp;
