import * as React from "react";
import { HashRouter as Router, Route } from "react-router-dom";

import StatusBadge from "../containers/StatusBadge";
import routes from "../router";
import NavBar from "./NavBar";

export default class MainComponent extends React.Component<undefined, undefined> {
  public render() {
      return (
        <Router>
          <div>
            <NavBar />
            <StatusBadge />
            <div className="container">
              <div className="row">
                <div className="col s12">
                  {this.getContent()}
                </div>
              </div>
            </div>
          </div>
        </Router>
      );
  }

  private getContent(): JSX.Element {
    const content: JSX.Element[] = routes.map((route, index) => (
      <Route
        key={index}
        path={route.path}
        exact={route.exact}
        component={route.main}
      />
    ));
    return (<div>{content}</div>);
  }
}
