import * as React from "react";

import { ICategory } from "./interfaces";
import CategoriesList from "./list";

interface IProps {
    loadCategories: () => void;
    categories: ICategory[];
    createCategory: (title: string, onSuccess: () => void) => void;
    updateCategory: (category: ICategory, onSuccess?: () => void) => void;
    deleteCategory: (id: number) => void;
}

interface IState {
    showForm: boolean;
    title: string;
}

export default class Categories extends React.Component<IProps, IState> {
    constructor(props) {
        super(props);
        this.state = { showForm: false, title: "" };
        this.onTitleChange = this.onTitleChange.bind(this);
        this.saveCategory = this.saveCategory.bind(this);
        this.hideForm = this.hideForm.bind(this);
    }

    public componentDidMount() {
        this.props.loadCategories();
    }

    public render() {
        return (
            <div className="categories">
                <h1>Categories</h1>
                <div className="row">
                    <div className="col s6 offset-s3 center-align">
                        {this.state.showForm ? this.showForm() : this.showButton()}
                    </div>
                </div>
                <CategoriesList
                    categories={this.props.categories}
                    updateCategory={this.props.updateCategory}
                    deleteCategory={this.props.deleteCategory}
                />
            </div>
        );
    }

    private showButton(): JSX.Element {
        return (
            <button onClick={() => this.setState({showForm: true})} className="waves-effect waves-light btn">
                <i className="material-icons left">add</i>Add new
            </button>
        );
    }

    private showForm(): JSX.Element {
        return (
            <div className="card">
                <div className="card-content">
                    <span className="card-title">New Category</span>
                    <form className="inline-list-form">
                        <div className="input-field">
                            <input
                                onChange={this.onTitleChange}
                                autoFocus={true}
                                placeholder="Category title"
                                id="title"
                                type="text"
                                className="validate"
                            />
                        </div>
                    </form>
                </div>
                <div className="card-action">
                    <button onClick={this.saveCategory} className="btn waves-effect waves-light" type="submit">
                        Save <i className="material-icons left">save</i>
                    </button>
                    <button onClick={this.hideForm} className="btn waves-effect waves-light">
                        Cancel <i className="material-icons left">close</i>
                    </button>
                </div>
            </div>
        );
    }

    private onTitleChange(e) {
        this.setState({title: e.target.value});
    }

    private saveCategory(e): void {
        e.preventDefault();

        this.props.createCategory(this.state.title, () => {
            this.setState({showForm: false});
        });
    }

    private hideForm(e): void {
        e.preventDefault();
        this.setState({ showForm: false, title: "" });
    }
}
