import $ from "jquery";
import * as _ from "lodash";
import * as React from "react";

import Category from "./category";
import { ICategory } from "./interfaces";

interface IProps {
    categories: ICategory[];
    updateCategory: (category: ICategory, onSuccess?: () => void) => void;
    deleteCategory: (id: number) => void;
}

export default class CategoriesList extends React.Component<IProps, undefined> {
    constructor(props) {
        super(props);
        this.getCategoriesList = this.getCategoriesList.bind(this);
        this.closeCollapsible = this.closeCollapsible.bind(this);
    }

    public componentDidMount() {
        $(this.refs.collapsibleContainer).collapsible();
    }

    public render(): JSX.Element {
        return (
            <ul className="collapsible popout" ref="collapsibleContainer">
                {this.getCategoriesList()}
            </ul>
        );
    }

    private closeCollapsible(index) {
        $(this.refs.collapsibleContainer).collapsible("close", index);
    }

    private getCategoriesList(): JSX.Element[] {
        let i = 0;
        return _.map(_.sortBy(this.props.categories, "title"), (cat) =>
            (
            <Category
                key={`category-${cat.id}`}
                category={cat}
                onSave={this.props.updateCategory}
                onDelete={this.props.deleteCategory}
                closeCollapsible={this.closeCollapsible}
                elementIndex={i++}
            />),
        );
    }
}
