import * as React from "react";

import { ICategory } from "./interfaces";

interface IProps {
    category: ICategory;
    onSave: (category: ICategory, onSuccess?: () => void) => void;
    onDelete: (id: number) => void;
    closeCollapsible: (index: number) => void;
    elementIndex: number;
}

interface IState {
    title: string;
    showDeleteConfirm: boolean;
}

export default class Category extends React.Component<IProps, IState> {
    private baseState: IState;
    constructor(props) {
        super(props);
        this.baseState = { title: props.category.title, showDeleteConfirm: false };
        this.state = this.baseState;
        this.onTitleChange = this.onTitleChange.bind(this);
        this.undoChanges = this.undoChanges.bind(this);
        this.saveCategory = this.saveCategory.bind(this);
        this.deleteCategory = this.deleteCategory.bind(this);
    }

    public render(): JSX.Element {
        return (
            <li>
                <div className="collapsible-header">{this.getLineContent()}</div>
                <div className="collapsible-body grey lighten-4">
                    <div className="row">
                        <div className="col s6">
                            {this.getFormContent()}
                        </div>
                        <div className="col s6">
                            <div className="row">
                                <div className="col s12 right-align category-detail-actions">
                                    <h5>More Actions</h5>
                                    {this.showDeleteButton()}
                                    {this.showDeleteConfirmButton()}
                                    <button onClick={this.undoChanges} className="btn waves-effect waves-light">
                                        Cancel / Undo <i className="material-icons left">undo</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        );
    }

    private showDeleteButton(): JSX.Element {
        if (this.state.showDeleteConfirm) {
            return <span />;
        }
        return (
            <button onClick={() => (this.setState({showDeleteConfirm: true}))} className="btn waves-effect waves-light">
                Delete category <i className="material-icons left">delete</i>
            </button>
        );
    }

    private showDeleteConfirmButton(): JSX.Element {
        if (!this.state.showDeleteConfirm) {
            return <span />;
        }
        return (
            <button onClick={this.deleteCategory} className="btn waves-effect waves-light red darken-4">
                Are you sure? <i className="material-icons left">delete</i>
            </button>
        );
    }

    private getLineContent(): JSX.Element {
        const {title} = this.props.category;
        return (
            <span style={{ width: "100%" }}>
                <h4>{title}</h4>
                <small className="right">Click to show options</small>
            </span>
        );
    }

    private getFormContent(): JSX.Element {
        return (
            <form className="inline-list-form">
                <h5>Update the category</h5>
                <input
                    onChange={this.onTitleChange}
                    autoFocus={true}
                    value={this.state.title}
                    id="title"
                    type="text"
                    className="validate"
                />
                <button onClick={this.saveCategory} className="btn waves-effect waves-light" type="submit">
                    Update <i className="material-icons left">save</i>
                </button>
            </form>
        );
    }

    private undoChanges(e): void {
        e.preventDefault();
        this.props.closeCollapsible(this.props.elementIndex);
        this.setState(this.baseState);
    }

    private saveCategory(e): void {
        e.preventDefault();
        const category: ICategory = {
            id: this.props.category.id,
            title: this.state.title,
        };
        this.props.onSave(category, () => (this.props.closeCollapsible(this.props.elementIndex)));
    }

    private deleteCategory(): void {
        this.props.onDelete(this.props.category.id);
    }

    private onTitleChange(e) {
        this.setState({title: e.target.value});
    }
}
