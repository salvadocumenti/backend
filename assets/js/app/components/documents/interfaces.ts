import { ICategory } from "../categories/interfaces";

export interface IAmount {
    amount: number;
    currency: string;
}

interface IDocumentCommon {
    id: number | null;
    title: string;
    description: string;
    date: string;
    category_id: number;
    attachment: string;
    attachment_thumb: string;
}

export interface IDocument extends IDocumentCommon {
    amount: IAmount;
    category: ICategory;
}

export interface IDocumentForm extends IDocumentCommon {
    amount: string;
}
