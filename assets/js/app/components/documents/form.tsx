import $ from "jquery";
import * as _ from "lodash";
import * as React from "react";

import { ICategory } from "../categories/interfaces";
import { IDocument, IDocumentForm } from "./interfaces";

interface IProps {
    document: IDocument | null;
    action: string;
    categories: ICategory[];
    onSave: (document: IDocumentForm) => void;
}

interface IState {
    document: IDocumentForm;
}

export default class extends React.Component<IProps, IState> {
    private selectInput;
    private titles = {
        create: {
            buttonText: "Save",
            formTitle: "Create a new document",
        },
        update: {
            buttonText: "Update",
            formTitle: "Update the document",
        },
    };

    constructor(props) {
        super(props);
        this.state = {
            document: {
                amount: props.document ? (parseInt(props.document.amount.amount, 10) / 100).toString() : "",
                category_id: props.document ? props.document.category_id : null,
                date: props.document ? props.document.date : "",
                description: props.document ? props.document.description : "",
                id: props.document ? props.document.id : null,
                title: props.document ? props.document.title : "",
            },
        };
        this.getTextField = this.getTextField.bind(this);
        this.getDateField = this.getDateField.bind(this);
        this.getSelectField = this.getSelectField.bind(this);
        this.saveForm = this.saveForm.bind(this);
    }

    public componentDidMount() {
        $(".datepicker").pickadate({
            clear: "Clear",
            close: "Ok",
            closeOnSelect: false, // Close upon selecting a date,
            formatSubmit: "yyyy-mm-dd",
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15, // Creates a dropdown of 15 years to control year,
            today: "Today",
        });
    }

    public render() {
        return (
            <form className="inline-list-form">
                <h5>{this.titles[this.props.action].formTitle}</h5>
                {this.getTextField("document", "title", true)}
                {this.getTextField("document", "description")}
                {this.getDateField("document", "date")}
                {this.getSelectField(this.props.categories, "document", "category_id")}
                {this.getTextField("document", "amount")}
                <button onClick={this.saveForm} className="btn waves-effect waves-light" type="submit">
                    {this.titles[this.props.action].buttonText} <i className="material-icons left">save</i>
                </button>
            </form>
        );
    }

    private saveForm(e) {
        e.preventDefault();
        this.props.onSave(this.state.document);
    }

    private getDateField(entity: string, fieldName: string, autofocus: boolean = false) {
        return this.getTextField(entity, fieldName, autofocus, "datepicker");
    }

    private getTextField(
        entity: string,
        fieldName: string,
        autofocus: boolean = false,
        className: string = "validate",
    ): JSX.Element {
        const id = `${entity}-${fieldName}`;
        const value = this.state[entity][fieldName];
        return (
            <div className="row input-field">
                <input
                    onChange={this.onFieldChange.bind(this, entity, fieldName)}
                    autoFocus={autofocus}
                    value={value}
                    id={id}
                    type="text"
                    className={className}
                    data-value={value}
                />
                <label htmlFor={id} className="active">{_.capitalize(fieldName)}</label>
            </div>
        );
    }

    private getSelectField(collection: ICategory[], entity: string, fieldName: string): JSX.Element {
        const id = `${entity}-${fieldName}`;
        return (
            <div className="row">
                <select
                    className="browser-default"
                    ref={(s) => this.selectInput = s}
                    value={this.state[entity][fieldName]}
                    onChange={this.onFieldChange.bind(this, entity, fieldName)}
                    id={id}
                >
                    <option value={null} disabled={true}>Choose your option</option>
                    {this.getCategoriesOptions(collection)}
                </select>
                <label htmlFor={id} className="active">{_.capitalize(fieldName)}</label>
            </div>
        );
    }

    private getCategoriesOptions(collection: ICategory[]) {
        return _.map(collection, (item) => (
            <option
                key={item.id}
                value={item.id}
            >
                {item.title}
            </option>
        ));
    }

    private onFieldChange(entity: string, fieldName: string, e) {
        const state = _.cloneDeep(this.state);
        state[entity][fieldName] = e.target.value;
        this.setState(state);
    }
}
