import $ from "jquery";
import * as _ from "lodash";
import * as React from "react";

import { ICategory } from "../categories/interfaces";
import Document from "./document";
import { IDocument, IDocumentForm } from "./interfaces";

interface IProps {
    documents: IDocument[];
    categories: ICategory[];
    updateDocument: (document: IDocumentForm, onSuccess?: () => void) => void;
    // deleteDocument: (id: number) => void;
}

export default class DocumentsList extends React.Component<IProps, undefined> {
    constructor(props) {
        super(props);
        this.getDocumentsList = this.getDocumentsList.bind(this);
        this.closeCollapsible = this.closeCollapsible.bind(this);
    }

    public componentDidMount() {
        $(this.refs.collapsibleContainer).collapsible();
    }

    public render(): JSX.Element {
        return (
            <ul className="collapsible popout" ref="collapsibleContainer">
                {this.getDocumentsList()}
            </ul>
        );
    }

    private closeCollapsible(index) {
        $(this.refs.collapsibleContainer).collapsible("close", index);
    }

    private getDocumentsList(): JSX.Element[] {
        let i = 0;
        return _.map(_.sortBy(this.props.documents, "date"), (document: IDocument) =>
            (
            <Document
                key={`document-${document.id}`}
                document={document}
                categories={this.props.categories}
                closeCollapsible={this.closeCollapsible}
                elementIndex={i++}
                onSave={this.props.updateDocument}
            />),
        );
    }
}
