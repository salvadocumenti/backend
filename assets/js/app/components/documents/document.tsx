// import * as _ from "lodash";
import * as moment from "moment";
import * as React from "react";

import { printAmount } from "../../utils";
import { ICategory } from "../categories/interfaces";
import DocumentForm from "./form";
import { IDocument, IDocumentForm } from "./interfaces";

interface IProps {
    document: IDocument;
    categories: ICategory[];
    onSave: (document: IDocumentForm, onSuccess?: () => void) => void;
    // onDelete: (id: number) => void;
    closeCollapsible: (index: number) => void;
    elementIndex: number;
}

interface IState {
    title: string;
    showDeleteConfirm: boolean;
}

export default class Document extends React.Component<IProps, IState> {
    private baseState: IState;
    constructor(props) {
        super(props);
        this.baseState = { title: props.document.title, showDeleteConfirm: false };
        this.state = this.baseState;
        this.undoChanges = this.undoChanges.bind(this);
        this.saveDocument = this.saveDocument.bind(this);
        this.deleteDocument = this.deleteDocument.bind(this);
    }

    public render(): JSX.Element {
        return (
            <li>
                <div className="collapsible-header">{this.getLineContent()}</div>
                <div className="collapsible-body grey lighten-4">
                    {this.getCollapsibleContent()}
                </div>
            </li>
        );
    }

    private getCollapsibleContent() {
        return (
            <div className="row">
                <div className="col s6">
                    {this.getFormContent()}
                </div>
                <div className="col s6">
                    <div className="row">
                        <div className="col s12 right-align document-detail-actions">
                            <h5>More Actions</h5>
                            {this.showDeleteButton()}
                            {this.showDeleteConfirmButton()}
                            <button onClick={this.undoChanges} className="btn waves-effect waves-light">
                                Cancel / Undo <i className="material-icons left">undo</i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    private showDeleteButton(): JSX.Element {
        if (this.state.showDeleteConfirm) {
            return <span />;
        }
        return (
            <button onClick={() => (this.setState({showDeleteConfirm: true}))} className="btn waves-effect waves-light">
                Delete document <i className="material-icons left">delete</i>
            </button>
        );
    }

    private showDeleteConfirmButton(): JSX.Element {
        if (!this.state.showDeleteConfirm) {
            return <span />;
        }
        return (
            <button onClick={this.deleteDocument} className="btn waves-effect waves-light red darken-4">
                Are you sure? <i className="material-icons left">delete</i>
            </button>
        );
    }

    private getLineContent(): JSX.Element {
        const {title, description, date, amount, category, attachment_thumb} = this.props.document;
        let attachmentTag: JSX.Element | null = null;
        if (attachment_thumb) {
            attachmentTag = <img src={attachment_thumb} className="attachment-thumb" />;
        }
        return (
            <span style={{ width: "100%" }}>
                {attachmentTag}
                <h4>{title}</h4>
                <p>{description}</p>
                <p>{moment.utc(date).format("DD/MM/YYYY")} - {printAmount(amount)} - {category.title}</p>
                <small className="right">Click to show options</small>
            </span>
        );
    }

    private getFormContent(): JSX.Element {
        return (
            <DocumentForm
                document={this.props.document}
                categories={this.props.categories}
                action={"update"}
                onSave={this.saveDocument}
            />
        );
    }

    private undoChanges(e): void {
        e.preventDefault();
        this.props.closeCollapsible(this.props.elementIndex);
        this.setState(this.baseState);
    }

    private saveDocument(document: IDocumentForm): void {
        this.props.onSave(document, () => (this.props.closeCollapsible(this.props.elementIndex)));
    }

    private deleteDocument(): void {
        // this.props.onDelete(this.props.document.id);
    }
}
