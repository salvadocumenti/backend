import * as React from "react";

import { ICategory } from "../categories/interfaces";
import { IDocument, IDocumentForm } from "./interfaces";
import DocumentsList from "./list";

interface IProps {
    loadDocuments: () => void;
    loadCategories: () => void;
    documents: IDocument[];
    categories: ICategory[];
    // createDocument: (title: string, onSuccess: () => void) => void;
    updateDocument: (document: IDocumentForm, onSuccess?: () => void) => void;
    // deleteDocument: (id: number) => void;
}

interface IState {
    showForm: boolean;
    title: string;
}

export default class Documents extends React.Component<IProps, IState> {
    constructor(props) {
        super(props);
        this.state = { showForm: false, title: "" };
        this.onTitleChange = this.onTitleChange.bind(this);
        this.saveDocument = this.saveDocument.bind(this);
        this.hideForm = this.hideForm.bind(this);
    }

    public componentDidMount() {
        this.props.loadDocuments();
        this.props.loadCategories();
    }

    public render() {
        return (
            <div className="documents">
                <h1>Documents</h1>
                <div className="row">
                    <div className="col s6 offset-s3 center-align">
                        {this.state.showForm ? this.showForm() : this.showButton()}
                    </div>
                </div>
                <DocumentsList
                    documents={this.props.documents}
                    categories={this.props.categories}
                    updateDocument={this.props.updateDocument}
                />
            </div>
        );
    }

    private showButton(): JSX.Element {
        return (
            <button onClick={() => this.setState({showForm: true})} className="waves-effect waves-light btn">
                <i className="material-icons left">add</i>Add new
            </button>
        );
    }

    private showForm(): JSX.Element {
        return (
            <div className="card">
                <div className="card-content">
                    <span className="card-title">New Document</span>
                    <form className="inline-list-form">
                        <div className="input-field">
                            <input
                                onChange={this.onTitleChange}
                                autoFocus={true}
                                placeholder="Document title"
                                id="title"
                                type="text"
                                className="validate"
                            />
                        </div>
                    </form>
                </div>
                <div className="card-action">
                    <button onClick={this.saveDocument} className="btn waves-effect waves-light" type="submit">
                        Save <i className="material-icons left">save</i>
                    </button>
                    <button onClick={this.hideForm} className="btn waves-effect waves-light">
                        Cancel <i className="material-icons left">close</i>
                    </button>
                </div>
            </div>
        );
    }

    private onTitleChange(e) {
        this.setState({title: e.target.value});
    }

    private saveDocument(e): void {
        e.preventDefault();

        // this.props.createDocument(this.state.title, () => {
        //     this.setState({showForm: false});
        // });
    }

    private hideForm(e): void {
        e.preventDefault();
        this.setState({ showForm: false, title: "" });
    }
}
