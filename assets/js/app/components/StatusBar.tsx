import * as React from "react";

interface IStatus {
  loading: boolean;
}

interface IProps {
  status: IStatus;
}

export default class StatusBar extends React.Component<IProps, undefined> {
  public render(): JSX.Element {
    const status = this.props.status.loading ?
        (
        <div className="progress status-bar">
            <div className="indeterminate" />
        </div>) :
        "";
    return (
        <div className="status-bar-wrapper">
            {status}
        </div>
    );
  }
}
