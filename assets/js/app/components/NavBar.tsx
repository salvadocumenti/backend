import * as React from "react";
import { Link } from "react-router-dom";

const NavBar = () => (
  <nav>
    <div className="nav-wrapper">
      <div className="col s12">
        <ul id="nav-mobile" className="left hide-on-med-and-down">
          <li><Link to="/">Home</Link></li>
          <li><Link to="/categories">Categories</Link></li>
          <li><Link to="/documents">Documents</Link></li>
        </ul>
      </div>
    </div>
  </nav>
);

export default NavBar;
