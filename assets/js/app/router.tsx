import * as React from "react";

import Categories from "./containers/Categories";
import Documents from "./containers/Documents";
import Home from "./containers/Home";

const routes = [{
    exact: true,
    main: () => <Home />,
    path: "/",
}, {
    exact: false,
    main: () => <Categories />,
    path: "/categories",
}, {
    exact: false,
    main: () => <Documents />,
    path: "/documents",
}];

export default routes;
