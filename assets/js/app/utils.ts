import * as Money from "js-money";
import Materialize from "materialize-css";

import { IAmount } from "./components/documents/interfaces";
import { BASE_URL } from "./config";

interface IApiFetchOptions {
    body?: string;
    method?: string;
    errorCallback?: (error) => void;
}

export const apiFetch = (url: string, options?: IApiFetchOptions) => {
    const fetchOptions = {
        headers: new Headers({
            "Content-Type": "application/json",
        }),
    };
    if (options.body) {
        fetchOptions["body"] = options.body;
    }
    if (options.method) {
        fetchOptions["method"] = options.method;
    }
    return fetch(`${BASE_URL}${url}`, fetchOptions).then((response) => {
        if (!response.ok) {
            throw new Error(`(${response.status}) ${response.statusText}`);
        }
        if (response.status === 204) {
            return response;
        }
        return response.json();
    }).catch((error) => {
        if (options.errorCallback) {
            options.errorCallback(error);
        }
        throw error;
    });
};

export const showMessage = (message: string | JSX.Element, duration: number = 4000, callback?): void => (
    Materialize.toast(message, duration, "rounded", callback)
);

export const printAmount = (amount: IAmount): string => {
    const money = Money.fromInteger(amount.amount, amount.currency);
    const currency = Money[amount.currency];
    const symbol = currency ? currency.symbol : "";
    return `${symbol}${money.toString()}`;
};
