import { connect } from "react-redux";

import { createCategory, deleteCategory, loadCategories, updateCategory } from "../actions/categories";
import CategoriesPage from "../components/categories/index";
import { ICategory } from "../components/categories/interfaces";

const mapStateToProps = (state) => ({
  categories: state.categories,
});

const mapDispatchToProps = (dispatch) => ({
    createCategory: (title: string, onSuccess: () => void): void => dispatch(createCategory(title, onSuccess)),
    deleteCategory: (id: number): void => dispatch(deleteCategory(id)),
    loadCategories: (): void => dispatch(loadCategories()),
    updateCategory: (category: ICategory, onSuccess?: () => void): void => dispatch(updateCategory(category, onSuccess)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesPage);
