import { connect } from "react-redux";

import HomePage from "../components/HomePage";

const mapStateToProps = (state): object => ({
  status: state.status,
});

const mapDispatchToProps = (dispatch): object => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
