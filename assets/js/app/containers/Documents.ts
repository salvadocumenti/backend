import { connect } from "react-redux";

import { loadCategories } from "../actions/categories";
import { loadDocuments, updateDocument } from "../actions/documents";
import DocumentsPage from "../components/documents/index";
import { IDocumentForm } from "../components/documents/interfaces";

const mapStateToProps = (state) => ({
    categories: state.categories,
    documents: state.documents,
});

const mapDispatchToProps = (dispatch) => ({
    loadCategories: (): void => dispatch(loadCategories()),
    loadDocuments: (): void => dispatch(loadDocuments()),
    updateDocument: (doc: IDocumentForm, onSuccess?: () => void): void => dispatch(updateDocument(doc, onSuccess)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DocumentsPage);
