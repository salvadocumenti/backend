import { connect } from "react-redux";

import StatusBar from "../components/StatusBar";

// Duplication, see StatusIcon.tsx
interface IStatus {
  loading: boolean;
}

interface IProps {
  status: IStatus;
}

const mapStateToProps = (state): IProps => ({
  status: state.status,
});

export default connect(mapStateToProps)(StatusBar);
