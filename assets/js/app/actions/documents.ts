import * as _ from "lodash";
import { IDocumentForm } from "../components/documents/interfaces";
import { apiFetch, showMessage } from "../utils";
import { statusLoading, statusNotLoading } from "./status";

export const LOAD_DOCUMENTS = "LOAD_DOCUMENTS";
export const DOCUMENTS_LOADED = "DOCUMENTS_LOADED";

export const loadDocuments = () => ((dispatch) => {
    dispatch(statusLoading());
    return apiFetch(`/documents`, {
        errorCallback: (error) => showMessage("There has been a problem with your fetch operation: " + error.message),
    }).then(
        (resp) => dispatch(parseDocuments(resp.data)),
        (error) => dispatch(statusNotLoading()),
    );
});

export const updateDocument = (document: IDocumentForm, onSuccess?: () => void) => ((dispatch) => {
    dispatch(statusLoading());
    return apiFetch(`/documents/${document.id}`, {
        body: JSON.stringify({document}),
        errorCallback: (error) => showMessage("There has been a problem with your fetch operation: " + error.message),
        method: "PATCH",
    }).then(
        (resp) => {
            if (!_.isNil(onSuccess)) {
                onSuccess();
            }
            showMessage("Saved!", 2000);
            return dispatch(loadDocuments());
        },
        (error) => dispatch(statusNotLoading()),
    );
});

const parseDocuments = (documents) => (
    {
        documents,
        type: DOCUMENTS_LOADED,
    }
);
