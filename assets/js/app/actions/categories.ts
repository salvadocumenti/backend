import * as _ from "lodash";

import { ICategory } from "../components/categories/interfaces";
import { apiFetch, showMessage } from "../utils";
import { statusLoading, statusNotLoading } from "./status";

export const LOAD_CATEGORIES = "LOAD_CATEGORIES";
export const CATEGORIES_LOADED = "CATEGORIES_LOADED";

export const loadCategories = () => ((dispatch) => {
    dispatch(statusLoading());
    return apiFetch(`/categories`, {
        errorCallback: (error) => showMessage("There has been a problem with your fetch operation: " + error.message),
    }).then(
        (resp) => dispatch(parseCategories(resp.data)),
        (error) => dispatch(statusNotLoading()),
    );
});

export const createCategory = (title: string, onSuccess: () => void) => ((dispatch) => {
    dispatch(statusLoading());
    return apiFetch(`/categories`, {
        body: JSON.stringify({category: {title}}),
        errorCallback: (error) => showMessage("There has been a problem with your fetch operation: " + error.message),
        method: "POST",
    }).then(
        (resp) => {
            onSuccess();
            showMessage("Created!", 2000);
            return dispatch(loadCategories());
        },
        (error) => dispatch(statusNotLoading()),
    );
});

export const updateCategory = (category: ICategory, onSuccess?: () => void) => ((dispatch) => {
    dispatch(statusLoading());
    return apiFetch(`/categories/${category.id}`, {
        body: JSON.stringify({category: {title: category.title}}),
        errorCallback: (error) => showMessage("There has been a problem with your fetch operation: " + error.message),
        method: "PATCH",
    }).then(
        (resp) => {
            if (!_.isNil(onSuccess)) {
                onSuccess();
            }
            showMessage("Saved!", 2000);
            return dispatch(loadCategories());
        },
        (error) => dispatch(statusNotLoading()),
    );
});

export const deleteCategory = (id: number) => ((dispatch) => {
    dispatch(statusLoading());
    return apiFetch(`/categories/${id}`, {
        errorCallback: (error) => showMessage("There has been a problem with your fetch operation: " + error.message),
        method: "DELETE",
    }).then(
        (resp) => {
            showMessage("Deleted!", 2000);
            return dispatch(loadCategories());
        },
        (error) => dispatch(statusNotLoading()),
    );
});

const parseCategories = (categories) => (
    {
        categories,
        type: CATEGORIES_LOADED,
    }
);
