export const STATUS_LOADING = "STATUS_LOADING";
export const STATUS_NOT_LOADING = "STATUS_NOT_LOADING";

export const statusLoading = (): object => ({
    type: STATUS_LOADING,
});

export const statusNotLoading = () => ({
    type: STATUS_NOT_LOADING,
});
